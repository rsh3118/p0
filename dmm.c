#include <stdio.h>  // needed for size_t
#include <stdlib.h> //for exit
#include <sys/mman.h> // needed for mmap
#include <assert.h> // needed for asserts
#include "dmm.h"

#define HEADER_ALIGNED (ALIGN(sizeof(header)))

#define FOOTER_ALIGNED (ALIGN(sizeof(footer)))

#define EMPTY_CHUNK_ALIGNED (ALIGN(sizeof(header)) + ALIGN(sizeof(footer)))

#define TRUE 1

#define FALSE 0

#define FREELIST_TABLE_SIZE 25



/* You can improve the below metadata structure using the concepts from Bryant
 * and OHallaron book (chapter 9).
 */

typedef struct header_h {
  /* size_t is the return type of the sizeof operator. Since the size of an
   * object depends on the architecture and its implementation, size_t is used
   * to represent the maximum size of any object in the particular
   * implementation. size contains the size of the data object or the number of
   * free bytes
   */
  size_t size;
  struct header_h* next;
  struct header_h* prev;
} header;

typedef struct{
  size_t size;
} footer;

/*
freelist maintains all the blocks which are not in use; freelist is kept
sorted to improve coalescing efficiency
*/

static header* prologue = NULL;

/*
every index in the freelist_table array corresponds to a freelist that only has
freed chunks of size 2^(index)
*/
void* freelist_table[FREELIST_TABLE_SIZE];

/*
All internal functions
*/

static void    add_head(int power, header* new_head);
static bool    can_coalesce_left(header* chunk);
static bool    can_coalesce_right(header* chunk);
static void    carve(header* chunk_to_carve);
static header* coalesce(header* a, header* b);
static void*   dmalloc_test(size_t numbytes);
static header* fill_chunk(header* chunk, size_t size, header* prev, header* next);
static header* find_largest_chunk(int min_power);
static char*   get_allocation_str(header* chunk);
static header* get_chunk(size_t size);
static size_t  get_chunk_size(header* chunk);
static footer* get_footer_ptr(header* chunk);
static size_t  get_footer_size(header* chunk);
static header* get_left_neighbor(header* chunk);
static header* get_right_neighbor(header* chunk);
static void    initialize_freelist_table();
static void    insert_middle_chunk(header* left, header* middle, header* right);
static bool    is_allocated(header* chunk);
static bool    is_epilogue(header* chunk);
static bool    is_logue(header* chunk);
static bool    is_prologue(header* chunk);
static int     log_2(int num);
static header* max_chunk(header* a, header* b);
static int     max_fit(size_t size);
static header* min_chunk(header* a, header* b);
static int     min_fit(size_t size);
static void    print_chunk(char* chunk_name, header* chunk);
static void    print_freelist_table();
static header* remove_head(int power);
static size_t  round_to_even(size_t num);
static void    set_allocation(header* chunk, bool allocation_status);
static void    set_chunk_size(header* chunk, size_t size);
static header* shift(header* chunk, int shift);
static header* split(header* chunk, size_t bytes_needed);
static int     two_to_the(int power);
static void    update_prev(header* chunk);

/*
adds head to the freelist that holds free chunks of size 2^(power)
*/
void add_head(int power, header* new_head){
  if(freelist_table[power] == NULL){
    freelist_table[power] = new_head;
  }else{
    header* old_head = (header*) freelist_table[power];
    if(power == 23){
      //print_chunk("old_head", old_head);
      //print_chunk("new_head", new_head);
    }
    old_head->prev = new_head;
    new_head->next = old_head;
    freelist_table[power] = new_head;
    if(power == 23){
      //print_freelist(freelist_table[power]);
    }
  }
}

/*
Checks if the left neighbor is the previous chunk
*/
bool can_coalesce_left(header* chunk){
  header* left_neighbor = get_left_neighbor(chunk);
  if(chunk == left_neighbor->next && !is_prologue(left_neighbor)){
    return true;
  }
  return false;
}

/*
Checks if the right neighbor is the next chunk
*/
bool can_coalesce_right(header* chunk){
  header* right_neighbor = get_right_neighbor(chunk);
  if(chunk == right_neighbor->prev && !is_epilogue(right_neighbor)){
    return true;
  }
  return false;
}

/*
carves up the chunk of memory into chunks for the seperate lists
*/
void carve(header* chunk_to_carve){
  //size_t space_used = 0;
  //print_chunk("chunk_to_carve", chunk_to_carve);
  void* memory_slab_addr = (void*) chunk_to_carve;
  //DEBUG("memory_slab_addr: %d", (int) memory_slab_addr);
  size_t  memory_slab_size = EMPTY_CHUNK_ALIGNED + get_chunk_size(chunk_to_carve);
  //size_t full_memory_slab_size = memory_slab_size;
  //DEBUG("memory_slab_size: %lu", memory_slab_size);
  while(true){
    //DEBUG("max_fit returned %d", max_fit(memory_slab_size));
    if(!(max_fit(memory_slab_size) > 0)){
      break;
    }
    int max_fit_power = max_fit(memory_slab_size);
    size_t max_chunk_size = (size_t) two_to_the(max_fit_power);
    header* carved_out_chunk = memory_slab_addr;
    size_t max_chunk_space = max_chunk_size + EMPTY_CHUNK_ALIGNED;
    memory_slab_addr += max_chunk_space;
    memory_slab_size -= max_chunk_space;
    fill_chunk(carved_out_chunk, max_chunk_size, NULL, NULL);
    add_head(max_fit_power, carved_out_chunk);
    //space_used += max_chunk_size + EMPTY_CHUNK_ALIGNED;
    //DEBUG("For power = %d", max_fit_power);
    //DEBUG("\t Used %lu/%lu", space_used, full_memory_slab_size);
    //print_chunk("Current carved out chunk", carved_out_chunk);
    //print_freelist(freelist_table[23]);
    //print_chunk("carved_out_chunk", carved_out_chunk);
  }
  DEBUG("fragmented memory: %lu", memory_slab_size);


  // while(true){
  //   DEBUG("max_fit returned %d", max_fit(chunk_to_carve->s2ize));
  //   if(!(max_fit(chunk_to_carve->s2ize) > 0)){
  //     break;
  //   }
  //   int max_fit_power = max_fit(chunk_to_carve->si2ze);
  //   size_t max_chunk_size = (size_t) two_to_the(max_fit_power);
  //   header* carved_out_chunk = chunk_to_carve;
  //   chunk_to_carve = split(chunk_to_carve, max_chunk_size);
  //   fill_chunk(carved_out_chunk, max_chunk_size, NULL, NULL);
  //   add_head(max_fit_power, carved_out_chunk);
  //   //print_chunk("carved_out_chunk", carved_out_chunk);
  // }
  // return chunk_to_carve;
}

/*
combines blocks a and b, the combined block is the block with the lower addr.
*/
header* coalesce(header* a, header* b){
  // chunk they are combined into
  header* coalesced_chunk = min_chunk(a,b);

  //chunk thats left over
  header* husk_chunk = max_chunk(a,b);
  /*
  In order to coalesce we need to remove the coalesced chunks footer, increase
  the coalesced chunks size and remove the husk_chunk header we also need to
  update the linked lists that contain the chunk getting coalesced

  Because the user can make no assumptions about the memory its being given we
  actually dont need to delete anything we just need to change the size

  OG CC: size,next,prev,...,footer
  OG HC: size,next,prev,...,footer

  (Assuming HC was the next)

  N CC: OGCC->s2ize,OGHC->next,OGCC->prev,...,OGHC->footer
  */
  /*
  coalesced_chunk->s2ize = original size inscreased by the size of its lost
  footer and the hunk's lost header and the husk chunks orginal size
  */
  size_t coalesced_chunk_size = get_chunk_size(coalesced_chunk) + get_chunk_size(husk_chunk) + FOOTER_ALIGNED + HEADER_ALIGNED;
  fill_chunk(coalesced_chunk, coalesced_chunk_size, coalesced_chunk->prev, husk_chunk->next);
  return coalesced_chunk;
}

/*
frees an allocated pointer
*/
void dfree(void* ptr) {
  /*
  we gave the user the space after the header so it makes sense that they give
  us back the same space and we have to subtract the size of the header from it
  to arrive back at the original header
  */
  header* freed_chunk = ptr - HEADER_ALIGNED;
  // DEBUG("\tfreed chunk address %d\n",
  // freed_chunk);
  //print_chunk("freed_chunk", freed_chunk);

  header* left_chunk;
  header* right_chunk;
  header* current = prologue;
  int found_left = false;
  while(!found_left) {
    //print_chunk("Node", freelist_head);
    if(current < freed_chunk && current->next > freed_chunk){
      left_chunk = current;
      right_chunk = current->next;
      //print_chunk("left chunk", left_chunk);
      //print_chunk("right chunk", right_chunk);
      found_left = true;
    }else{
      current = current->next;
    }
  }

  insert_middle_chunk(left_chunk, freed_chunk, right_chunk);

  //print_freelist();

  if(can_coalesce_right(freed_chunk)){
    //DEBUG("Trying to coalesce right");
    //print_chunk("freed_chunk", freed_chunk);
    //print_chunk("right_neighbor", get_right_neighbor(freed_chunk));
    coalesce(freed_chunk, get_right_neighbor(freed_chunk));
    //print_freelist(prologue);
  }

  if(can_coalesce_left(freed_chunk)){
    //DEBUG("Trying to coalesce left");
    //print_chunk("freed_chunk", freed_chunk);
    //print_chunk("left_neighbor", get_left_neighbor(freed_chunk));
    coalesce(freed_chunk, get_left_neighbor(freed_chunk));
    //print_freelist(prologue);
  }
}

/*
Finds a chuck of data in the freelist large enough to accomadate the requested
size
*/
void* dmalloc(size_t numbytes) {
  //printf("Requested a chunk of size %lu \n", numbytes);
  /* initialize through sbrk call first time */
  if(prologue == NULL) {
    if(!dmalloc_init())
      return NULL;
  }

  assert(numbytes > 0);

  /* your code here */

  // So basically at this point the programmer has asked for [numbytes] amount
  // of space

  //We need to figure out which freelist to check

  int min_power = min_fit(numbytes);
  //DEBUG("The smallest chunk that will fit %lu bytes is of size %lu", numbytes, (size_t) two_to_the(min_power));

  //print_freelist_table();

  //lets check if there is an available freed chunk
  header* allocated_chunk;

  if(freelist_table[min_power] == NULL){
    //DEBUG("There is no free chunk that is of size %lu bytes so we have to get something larger", (size_t) two_to_the(min_power));
    // we are guranteed that if largest_chunk is non null it is has a greater
    // size than 2^(min_power) bytes
    header* largest_chunk = find_largest_chunk(min_power);
    remove_head(log_2((int) get_chunk_size(largest_chunk)));
    //DEBUG("The largest chunk we could find is of size %lu", largest_chunk->si2ze);
    allocated_chunk = largest_chunk;
    size_t allocated_chunk_size = (size_t) two_to_the(min_power);
    header* husk_chunk = split(largest_chunk, allocated_chunk_size);
    //print_chunk("husk_chunk", husk_chunk);
    fill_chunk(allocated_chunk, allocated_chunk_size, NULL, NULL);
    carve(husk_chunk);
    //print_freelist(freelist_table[23]);
    //print_freelist_table();
  }else{
    //DEBUG("We found a chunk of size %lu bytes", (size_t) two_to_the(min_power));
    allocated_chunk = remove_head(min_power);
    size_t allocated_chunk_size = (size_t) two_to_the(min_power);
    fill_chunk(allocated_chunk, allocated_chunk_size, NULL, NULL);
  }

  //print_chunk("allocated_chunk", allocated_chunk);
  //print_freelist_table();
  return ((void*) allocated_chunk) + HEADER_ALIGNED;
  // //print_freelist(prologue);
  //
  // /*
  // we need to give the user the space only after the header so they dont change
  // the header
  // */
  //
  // return ((void*) allocated_chunk) + HEADER_ALIGNED;
  // return NULL;
  //
  // //print_freelist(prologue);
  //
  // // So basically at this point the programmer has asked for [numbytes] amount
  // // of space
  // /*
  // Want to see where in the freelist we can add it
  // */
  // header* chunk_to_split = get_chunk(numbytes);
  //
  // //print_chunk("chunk_to_split", chunk_to_split);
  //
  // split(chunk_to_split,numbytes);
  //
  // header* allocated_chunk = chunk_to_split;
  //
  // fill_chunk(allocated_chunk, numbytes, NULL, NULL);
  //
  // //print_freelist(prologue);
  //
  // /*
  // we need to give the user the space only after the header so they dont change
  // the header
  // */
  //
  // return ((void*) allocated_chunk) + HEADER_ALIGNED;
  // return NULL;

}

/*
initializes the freelist(s)
*/
bool dmalloc_init() {


  /* Two choices:
   * 1. Append prologue and epilogue blocks to the start and the
   * end of the freelist
   *
   * 2. Initialize freelist pointers to NULL
   *
   * Note: We provide the code for 2. Using 1 will help you to tackle the
   * corner cases succinctly.
   */

  size_t max_bytes = ALIGN(MAX_HEAP_SIZE);
  /* returns heap_region, which is initialized to freelist */
  prologue = (header*) mmap(NULL, max_bytes, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  /* Q: Why casting is used? i.e., why (void*)-1? */

  if (prologue == (void *)-1){
    return false;
  }

  //parameters for calculcations

  void* prologue_address = (void*) prologue;
  size_t slab_size = max_bytes - 3*EMPTY_CHUNK_ALIGNED;

  //Create prologue and epilogue

  header* slab = prologue_address + EMPTY_CHUNK_ALIGNED;
  header* epilogue = prologue_address + max_bytes - EMPTY_CHUNK_ALIGNED;

  // Fill chunks

  fill_chunk(prologue, 0, NULL, slab);
  fill_chunk(slab, slab_size, prologue, epilogue);
  fill_chunk(epilogue, 0, slab, NULL);


  initialize_freelist_table();

  carve(slab);

  //print_freelist_table();

  return true;
}

/*
debugging function for testing dmalloc piece by piece
*/
void* dmalloc_test(size_t numbytes) {
  //printf("Requested a chunk of size %lu \n", numbytes);
  /* initialize through sbrk call first time */
  if(prologue == NULL) {
    if(!dmalloc_init())
      return NULL;
  }

  assert(numbytes > 0);

  /* your code here */

  print_freelist(prologue);

  // So basically at this point the programmer has asked for [numbytes] amount
  // of space
  /*
  Want to see where in the freelist we can add it
  */
  header* chunk_to_split = get_chunk(numbytes);
  print_chunk("chunk_to_split", chunk_to_split);
  //
  // /*
  // left chunk is what the previous of chunk_to_split was
  // */
  //
  // header* left_chunk = chunk_to_split-> prev;
  //
  // print_chunk("left chunk", left_chunk);
  //
  // /*
  // right chunk is what is leftover from chunk_to_split after slicing off the
  // new chunk from the left side of chunk_to_split
  // */
  //
  // header* right_chunk = split(chunk_to_split,numbytes);
  //
  // print_chunk("right_chunk", right_chunk);
  //
  // /*
  // middle chunk is the new chunk that got sliced off
  // */
  //
  // header* middle_chunk = chunk_to_split;
  //
  // fill_chunk(middle_chunk, numbytes, NULL, NULL);
  //
  // insert_middle_chunk(left_chunk, middle_chunk, right_chunk);
  //
  // print_freelist(prologue);
  //
  // /*
  // we need to give the user the space only after the header so they dont change
  // the header
  // */
  //
  //
  // return ((void*) middle_chunk) + HEADER_ALIGNED;
  return NULL;

}

/*
fills chunk with all its parameters
*/
header* fill_chunk(header* chunk, size_t size, header* prev, header* next){
  chunk-> size = size;
  chunk-> prev = prev;
  chunk-> next = next;
  int int_size = (int) chunk->size;
  footer* ftr_addr = ((void*) chunk) + int_size + HEADER_ALIGNED;
  ftr_addr-> size = size;
  return chunk;
}

/*
returns the largest chunk size that can accomadate 2^(min_power) bytes within
the freelist table
*/
header* find_largest_chunk(int min_power){
  // max power (rigth end of the array)
  int power = 24;
  while(power > min_power){
    if(freelist_table[power] != NULL){
      return (header*) freelist_table[power];
    }else{
      power -= 1;
    }
  }
  return NULL;
}

/*
returns "free" if the chunk is free or "alloc" if the chunk is allocated
*/
char* get_allocation_str(header* chunk){
  if(is_allocated(chunk)){
    return "alloc";
  }
  return "free";
}

/*
iterates through list and gets chunk of a certain size_t
*/
header* get_chunk(size_t size){
  header* current = prologue;
  while(current != NULL) {
    //print_chunk("Node", freelist_head);
    if(get_chunk_size(current) >= size){
      return current;
    }else{
      current = current->next;
    }
  }
  return NULL;
}

/*
calculates the true chunk size accounting for the fact that the last bit is
being used for storing the allocation status
*/
size_t get_chunk_size(header* chunk){
  if(is_allocated(chunk)){
    return chunk->size - 1;
  }
  return chunk->size;
}

/*
calculates the chunk's footer addr.
*/
footer* get_footer_ptr(header* chunk){
  return ((void*) chunk) + HEADER_ALIGNED + ((int) get_chunk_size(chunk));
}

/*
gets the size stored in the chunk's footer
*/
size_t get_footer_size(header* chunk){
  return (size_t) (round_to_even(get_footer_ptr(chunk)->size));
}

/*
calculates the address of the chunk's left neighbor
*/
header* get_left_neighbor(header* chunk){
  footer* left_neighbor_footer = ((void*) chunk) - FOOTER_ALIGNED;
  header* left_neighbor = ((void*) chunk) - HEADER_ALIGNED - round_to_even(get_footer_ptr(chunk)->size) - FOOTER_ALIGNED;
  return left_neighbor;
}

/*
calculates the address of the chunk's right neighbor
*/
header* get_right_neighbor(header* chunk){
  header* right_neighbor = ((void*) chunk) + HEADER_ALIGNED + get_chunk_size(chunk) + FOOTER_ALIGNED;
  return right_neighbor;
}

/*
sets up table of freelists (one freelist for every size)
*/
void initialize_freelist_table(){
  //set all freelists heads to NULL
  for(int power = 0; power < FREELIST_TABLE_SIZE; power++){
    freelist_table[power] = NULL;
  }
}

/*
Inserts the middle chunk between left and right
Should only be called when left's next is right and right's prev is left and
middle is to be inserted between them
neither left nor right should be NULL
*/
void insert_middle_chunk(header* left, header* middle, header* right){
  left->next = middle;
  right->prev = middle;
  middle->prev = left;
  middle->next = right;
}

/*
The chunk's allocation status (whether or not its allocated) is stored in the
last bit of its size (in both header and footer) what this translates to is that
by checking if the size is even or odd we know whether the last bit (allocation
status) is 1 or 0.

The reason storing the allocation status in the last bit is safe is because we
are guranteed (by the carve function) that all the bins are of even sizes 2, 4,
...2^(24) [i.e the zeroth bin is never used] so if we have size thats odd we
know its allocated and we can just subtract 1 to get the true size
*/
bool is_allocated(header* chunk){
  int rem = ((int) chunk->size) % 2;
  if(rem == 0){
    return false;
  }else{
    return true;
  }
}

/*
Thin wrapper around is_logue() for code clarity
*/
bool is_epilogue(header* chunk){
  return is_logue(chunk);
}

/*
checks if the chunk is the epilogue or prologue
*/
bool is_logue(header* chunk){
  return chunk->size == 0;
}

/*
think wrapper around is_logue for code clarity
*/
bool is_prologue(header* chunk){
  return is_logue(chunk);
}

/*
calculates log base 2
*/
static int log_2(int num){
  int power = 0;
  // temp will be multiplied by two until it is greater than or equal to num
  int temp = 1;
  while(temp*2 <= num){
    power += 1;
    temp *= 2;
  }
  return power;
}

/*
returns the chunk with the higher address out of a and b
*/
header* max_chunk(header* a, header* b){
  if(a > b){
    return a;
  }
  if( b > a){
    return b;
  }
  /* case in which we are trying to find the min of the same chunk accidentaly*/
  return NULL;
}

/*
finds the max size chunk size (expressed in 2^(max_size)) that can fit into the
size_t
*/
static int max_fit(size_t size){
  // account for space taken up by the chunk to be created header and footer
  size_t size_leftover = size - EMPTY_CHUNK_ALIGNED;

  int max_size = (size_t) log_2((int) size_leftover);

  return max_size;
}

/*
returns the chunk with the lower address out of a and b
*/
header* min_chunk(header* a, header* b){
  if(a < b){
    return a;
  }
  if( b < a){
    return b;
  }
  /* case in which we are trying to find the min of the same chunk accidentaly*/
  return NULL;
}

/*
finds the smallest chunk that can accomadate an allocation of size
*/
int min_fit(size_t size){
  //if we find the largest size chunk that can fit into size we agre guranteed
  //that the smallest size chunk that can fit size is at most one greater than
  //it

  // find the largest size chunk that can fit into size
  int min_size = log_2((int) size);

  //DEBUG("log_2(%lu) = %d", size, min_size);
  // if there is a part of the size we cannot accomadate than we have to expand
  // it in order to make it fit
  //DEBUG("%lu - two_to_the(%d) = %d > 0", size, min_size, (int) (size - two_to_the(min_size)));
  if(((int) (size - two_to_the(min_size))) > 0){
    min_size += 1;
  }

  return min_size;

}

/*
prints the chunk's name along with its parameters
*/
void print_chunk(char* chunk_name, header* chunk){
  DEBUG("  %5.5s %12.12s addr:%9d, prev:%9d, next:%9d, header-size:%9d, footer-size: %9d, space: %9d \n",
  get_allocation_str(chunk),
  chunk_name,
  (int) chunk,
  (int) chunk->prev,
  (int) chunk->next,
  (int) get_chunk_size(chunk),
  (int) get_footer_size(chunk),
  (int) EMPTY_CHUNK_ALIGNED + ((int) get_chunk_size(chunk)));
}

/*
prints the freelist
*/
void print_freelist(header* head) {
  header* current = head;
  while(current != NULL) {

    char* label = "|";

    //if the prev is null we know it is the head
    if(current->prev == NULL){
      label = "HEAD";
    }

    //if the next is null we know it is the tail
    if(current->next == NULL){
      label = "TAIL";
    }

    //if the prev and next is null we know it is the only one
    if(current->prev == NULL && current->next == NULL){
      label = "single";
    }
    print_chunk(label, current);
    current = current->next;
  }
  DEBUG("\n");
}

/*
prints the freetable
*/
void print_freelist_table(){
  for(int power = 0; power < FREELIST_TABLE_SIZE; power++){
    DEBUG("Freelist of size 2^%d chunks:\n", power);
    if(freelist_table[power] != NULL){
      print_freelist((header*) freelist_table[power]);
    }else{
      DEBUG("\t NULL\n");
    }
  }
}

/*
removes the head of the freelist that has chunks of 2^(power) size

DO NOT CALL THIS FUNCTION WITHOUT ENSURING THAT freelist_table[power] != NULL
*/
header* remove_head(int power){
  header* head = (header*) freelist_table[power];
  bool is_single = head->next == NULL;
  if(is_single){
    freelist_table[power] = NULL;
    return head;
  }else{
    header* next = head->next;
    next->prev = NULL;
    freelist_table[power] = next;
    return head;
  }
}

/*
rounds down to the closest even number
*/
size_t round_to_even(size_t num){
  if(num % 2 == 0){
    return num;
  }
  return num - 1;
}

/*
sets the last bit of the size in both the header and footer to the allocation
status
*/
void set_allocation(header* chunk, bool allocation_status){
  size_t true_size = get_chunk_size(chunk);
  size_t size_and_allocation = 0;
  if(allocation_status){
    size_and_allocation = true_size + 1;
  }else{
    size_and_allocation = true_size;
  }
  chunk-> size = size_and_allocation;
  int int_size = (int) true_size;
  DEBUG("True size is %d", int_size);
  footer* ftr_addr = ((void*) chunk) + int_size + HEADER_ALIGNED;
  ftr_addr-> size = size_and_allocation;
}

/*
sets the chunk size accounting for the fact that the the last bit is being used
to store the allocation status
*/
  void set_chunk_size(header* chunk, size_t size){
  size_t size_and_allocation = 0;
  if(is_allocated(chunk)){
    size_and_allocation = size + 1;
  }
  size_and_allocation = size;
  DEBUG("show size %lu", size_and_allocation);
  DEBUG("size_and_allocation is of size %lu", sizeof(size_and_allocation));
  DEBUG("chunk->size is of size %lu", sizeof(chunk->size));
  //DEBUG()
  print_chunk("chunk", chunk);
  chunk->size = size_and_allocation;
  // int int_size = (int) size;
  // footer* ftr_addr = ((void*) chunk) + int_size + HEADER_ALIGNED;
  // ftr_addr-> size = size_and_allocation;
}

/*
shifts a chunk over by a certain amount
*/
header* shift(header* chunk, int shift){
  //copy all the important stuff in the chunk
  header* prev = chunk->prev;
  header* next = chunk->next;
  size_t size = get_chunk_size(chunk);

  //shift the chunk
  chunk = ((void*) chunk) + shift;

  //fill the new chunk with the appropriate info
  fill_chunk(chunk, size - shift, prev, next);

  //update those who are storing the chunk's location with the new location
  update_prev(chunk);

  return chunk;
}

/*
returns the updated parent_chunk because the child_chunk pointer is just the Should
parent cunk pointer
*/
header* split(header* parent_chunk, size_t bytes_needed){
  //make space for the child
  int child_chunk_space = EMPTY_CHUNK_ALIGNED + bytes_needed;
  header* shifted_parent = shift(parent_chunk, child_chunk_space);

  return shifted_parent;
}

/*
return 2^(power)
*/
int two_to_the(int power){
  int temp_power = 0;
  int temp = 1;
  while(temp_power < power){
    temp *= 2;
    temp_power += 1;
  }
  return temp;
}

/*
updates the chunk that was previous in the linked list with the new address of
the chunk
*/
void update_prev(header* chunk){
  header* prev = chunk->prev;
  if(prev != NULL){
    prev->next = chunk;
  }
}







































int main(int argc, char *argv[]) {
  //Random Tests
  // //dmalloc_init();
  // //print_freelist_table();
  // size_t numbytes = 10;
  // //DEBUG("If we are trying to allocate %lu bytes then we need to use a chunk of size 2^%d", numbytes, min_fit(numbytes));
  // dmalloc(numbytes);
  dmalloc_init();
  header* sample_chunk = (header*) freelist_table[10];
  print_chunk("sample", sample_chunk);
  DEBUG("Chunk size is %lu", get_chunk_size(sample_chunk));
  set_allocation(sample_chunk, true);
  print_chunk("sample", sample_chunk);
  DEBUG("Chunk size is %lu", get_chunk_size(sample_chunk));
  set_chunk_size(sample_chunk, 2056);
  print_chunk("sample", sample_chunk);


  // Custom Test Cases
  // char *array1, *array2, *array3;
  // int i;
  //
  // printf("calling malloc(10)\n");
  // array1 = (char*)dmalloc(10);
  // if(array1 == NULL) {
  //   fprintf(stderr,"call to dmalloc() failed\n");
  //   fflush(stderr);
  //   exit(1);
  // }
  //
  // for(i=0; i < 9; i++) {
  //   array1[i] = 'a';
  // }
  // array1[9] = '\0';
  //
  // printf("String: %s\n",array1);
  //
  //
  // printf("calling malloc(10)\n");
  // array2 = (char*)dmalloc(10);
  // if(array2 == NULL) {
  //   fprintf(stderr,"call to dmalloc() failed\n");
  //   fflush(stderr);
  //   exit(1);
  // }
  //
  // for(i=0; i < 9; i++) {
  //   array2[i] = 'a';
  // }
  // array2[9] = '\0';
  //
  // printf("String: %s\n",array2);
  //
  // printf("calling malloc(10)\n");
  // array3 = (char*)dmalloc(10);
  // if(array3 == NULL) {
  //   fprintf(stderr,"call to dmalloc() failed\n");
  //   fflush(stderr);
  //   exit(1);
  // }
  //
  // for(i=0; i < 9; i++) {
  //   array3[i] = 'a';
  // }
  // array3[9] = '\0';
  //
  // printf("String: %s\n",array3);
  //
  // DEBUG("Just to make sure everything is intact");
  //
  // printf("String: %s\n",array1);
  // printf("String: %s\n",array2);
  // printf("String: %s\n",array3);
  //
  // print_freelist_table();

  //
  //
  //
  //
  // // printf("calling malloc(10)\n");
  // // array2 = (char*)dmalloc(10);
  // // if(array2 == NULL) {
  // //   fprintf(stderr,"call to dmalloc() failed\n");
  // //   fflush(stderr);
  // //   exit(1);
  // // }
  // //
  // // for(i=0; i < 9; i++) {
  // //   array2[i] = 'a';
  // // }
  // // array2[9] = '\0';
  // //
  // // printf("calling free(10)\n");
  // // dfree(array2);
  // dfree(array3);
  // dfree(array1);
  // dfree(array2);
  //



  // Standard Test Cases
  // char *array1, *array2, *array3;
  // int i;
  //
  // printf("calling malloc(10)\n");
  // array1 = (char*)dmalloc(10);
  // if(array1 == NULL) {
  //   fprintf(stderr,"call to dmalloc() failed\n");
  //   fflush(stderr);
  //   exit(1);
  // }
  //
  // for(i=0; i < 9; i++) {
  //   array1[i] = 'a';
  // }
  // array1[9] = '\0';
  //
  // printf("String: %s\n",array1);
  //
  // printf("calling malloc(940)\n");
  // array2 = (char*)dmalloc(940);
  // if(array2 == NULL) {
  //   fprintf(stderr,"call to dmalloc() failed\n");
  //   fflush(stderr);
  //   exit(1);
  // }
  //
  // for(i=0; i < 99; i++) {
  //   array2[i] = 'b';
  // }
  // array2[99] = '\0';
  //
  // printf("String : %s, %s\n",array1, array2);
  //
  // printf("calling free(940)\n");
  // dfree(array2);
  // printf("calling malloc(945)\n");
  // array3 = (char*)dmalloc(945);
  //
  // if(array3 == NULL) {
  //   fprintf(stderr,"call to dmalloc() failed\n");
  //   fflush(stderr);
  //   exit(1);
  // }
  // for(i=0; i < 945; i++) {
  //   array3[i] = 'c';
  // }
  // array3[945] = '\0';
  //
  // printf("String: %s, %s, %s\n",array1, array2, array3);
  //
  // printf("calling free(945)\n");
  // dfree(array3);
  //
  // printf("Basic testcases passed!\n");



  return(0);
}
